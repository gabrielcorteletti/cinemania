---
title: "Sobre"
date: 2021-10-18T13:59:36-03:00
image: /images/minerva.jpg
draft: false
---

### o Site:
Esse site foi elaborado utilizando o gerador de site estático Hugo como requisito avaliativo
para a disciplina PMR3304 - Sistemas de Informação, no curso de Engenharia Mecatrônica da Escola
Politécnica da Universidade de São Paulo.

### o Autor:
Site desenvolvido por Gabriel Antonio Corteletti Tápias, entusiasta de cinema e, atualmente (2021),
estudante do 3º ano de Engenharia Mecatrônica na EP-USP, NºUSP: 11258210. Informações de contato:

[LinkedIn](https://www.linkedin.com/in/gabriel-corteletti-t%C3%A1pias-98251120b/)

gabrielcorteletti@usp.br