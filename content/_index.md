---
title: ""
date: 2021-10-18T13:47:59-03:00
draft: false
---

Bem vindo ao CineMania, o mais novo blog de cinema. Aqui, são apresentadas as definições de alguns dos meus gêneros
cinematográficos favoritos. Caso se interesse por algum desses temas, acesse a seção "Favoritos", por meio da barra de navegação no topo da página, para conferir um dos meus filmes favoritos de cada gênero aqui apresentado.