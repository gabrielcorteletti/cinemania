---
title: "Faroeste"
date: 2021-10-21T10:14:50-03:00
image: /images/cartazes/era_uma_vez_no_oeste.jpg
draft: false
---

## Era uma Vez no Oeste (1968)

**Sinopse:**
Em virtude das terras que possuía serem futuramente a rota da estrada de ferro, um pai e todos os filhos são
brutalmente assassinados por um matador profissional. Entretanto, ninguém sabia que ele, viúvo há seis anos,
tinha se casado com uma outra mulher, de Nova Orleans, que passa ser a dona do local e recebe a proteção de um
hábil atirador, que tem contas a ajustar com o frio matador.

**Crítica:**
Uma obra-prima do "western spaghetti" de Sergio Leone com uma trilha sonora clássica de Morricone.

**fonte:** [Rotten Tomatoes](https://www.rottentomatoes.com/m/once_upon_a_time_in_the_west)