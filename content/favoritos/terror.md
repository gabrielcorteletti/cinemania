---
title: "Terror"
date: 2021-10-21T10:14:35-03:00
image: /images/cartazes/hereditario.jpg
draft: false
---

## Hereditário (2018)

**Sinopse:**
Após a morte da reclusa avó, a família Graham começa a desvendar algumas coisas. Mesmo após a partida da matriarca,
ela permanece como se fosse uma sombra sobre a família, especialmente sobre a solitária neta adolescente, Charlie,
por quem ela sempre manteve uma fascinação não usual. Com um crescente terror tomando conta da casa, a família explora
caminhos mais escuros para escapar do infeliz destino que herdaram.

**Crítica:**
Hereditário usa sua premissa clássica como estrutura para um filme de terror angustiante e notavelmente inquietante,
cujo impacto perdura muito além dos créditos finais.


**fonte:** [Rotten Tomatoes](https://www.rottentomatoes.com/m/hereditary)