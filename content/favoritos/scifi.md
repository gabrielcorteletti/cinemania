---
title: "Ficção Científica"
date: 2021-10-21T10:14:12-03:00
image: /images/cartazes/blade_runner.jpg
draft: false
---

## Blade Runner (1982)

**Sinopse:**
No século 21, uma corporação desenvolve clones humanos para serem usados como escravos em colônias fora da
Terra, identificados como replicantes. Em 2019, um ex-policial é acionado para caçar um grupo fugitivo vivendo
disfarçado em Los Angeles.

**Crítica:**
Incompreendido quando chegou aos cinemas, a influência do misterioso e neo-noir Blade Runner de Ridley Scott
se aprofundou com o tempo. Uma obra-prima de ficção científica visualmente notável e dolorosamente humana.

**fonte:** [Rotten Tomatoes](https://www.rottentomatoes.com/m/blade_runner)