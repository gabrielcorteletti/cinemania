---
title: "Guerra"
date: 2021-10-21T10:14:58-03:00
image: /images/cartazes/saving_private_ryan.jpg
draft: false
---

## O Resgate do Soldado Ryan (1998)

**Sinopse:**
Ao desembarcar na Normandia, no dia 6 de junho de 1944, o Capitão Miller recebe a missão de comandar um grupo do
Segundo Batalhão para o resgate do soldado James Ryan, o caçula de quatro irmãos, dentre os quais três morreram
em combate. Por ordens do chefe George C. Marshall, eles precisam procurar o soldado e garantir o seu retorno,
com vida, para casa.

**Crítica:**
Ancorado por outra performance fenomenal de Tom Hanks, o filme de guerra agressivamente realista de Steven
Spielberg praticamente redefine o gênero.

**fonte:** [Rotten Tomatoes](https://www.rottentomatoes.com/m/saving_private_ryan)